import { BetsApiPath } from '../../common/enums/enums';
import { isAuth } from '../../middlewares/auth.middleware';
import { betValidation } from '../../middlewares/validation/bet/bet.validation';

const initBet = (Router, services) => {
  const { betService } = services;
  const router = Router();

  router 
    .post(BetsApiPath.ROOT, betValidation.create, isAuth, (req, res, next) => betService
      .create(req.body)
      .then(data => res.send(data))
      .catch(next));

  return router;
};

export { initBet };
