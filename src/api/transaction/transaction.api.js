import { TransactionsApiPath } from '../../common/enums/enums';
import { isAuth, isAdmin } from '../../middlewares/auth.middleware';
import { transactionValidation } from '../../middlewares/validation/transaction/transaction.validation';

const initTransaction = (Router, services) => {
  const { transactionService } = services;
  const router = Router();

  router 
    .post(TransactionsApiPath.ROOT, transactionValidation.create, isAuth, isAdmin, (req, res, next) => transactionService
      .create(req.body)
      .then(data => res.send(data))
      .catch(next));

  return router;
};

export { initTransaction };
