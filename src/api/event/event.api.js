import { EventsApiPath } from '../../common/enums/enums';
import { isAuth, isAdmin } from '../../middlewares/auth.middleware';
import { eventValidation } from '../../middlewares/validation/event/event.validation';

const initEvent = (Router, services) => {
  const { eventService } = services;
  const router = Router();

  router 
    .post(EventsApiPath.ROOT, eventValidation.create, isAuth, isAdmin, (req, res, next) => eventService
      .create(req.body)
      .then(data => res.send(data))
      .catch(next));

  return router;
};

export { initEvent };
