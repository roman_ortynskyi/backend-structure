import { ApiPath } from '../common/enums/enums';
import { 
  userService, 
  transactionService, 
  eventService,
  betService 
} from '../services/services';
import { initEvent } from './event/event.api';
import { initTransaction } from './transaction/transaction.api';
import { initUser } from './user/user.api';
import { initBet } from './bet/bet.api';

// register all routes
const initApi = Router => {
  const apiRouter = Router();

  apiRouter.use(
    ApiPath.USERS,
    initUser(Router, {
      userService
    })
  );

  apiRouter.use(
    ApiPath.TRANSACTIONS,
    initTransaction(Router, {
      transactionService
    })
  );

  apiRouter.use(
    ApiPath.EVENTS,
    initEvent(Router, {
      eventService
    })
  );

  apiRouter.use(
    ApiPath.BETS,
    initBet(Router, {
      betService
    })
  );

  return apiRouter;
};

export { initApi };
