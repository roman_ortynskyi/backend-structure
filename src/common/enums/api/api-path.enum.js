const ApiPath = {
  USERS: '/users',
  TRANSACTIONS: '/transactions',
  EVENTS: '/events',
  BETS: '/bets'
};

export { ApiPath };
