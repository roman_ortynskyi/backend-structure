class TransactionService {
    constructor({ transactionRepository, userRepository }) {
        this._transactionRepository = transactionRepository;
        this._userRepository = userRepository;
    }

    async create(transaction) {
        const { userId, cardNumber, amount } = transaction;

        const user = await this._userRepository.getById(userId);
        
        const [result] = await this._transactionRepository.create({
            user_id: userId,
            card_number: cardNumber,
            amount
        });

        const currentBalance = amount + user.balance;
        await this._userRepository.updateById(userId, {
            balance: currentBalance
        });

        return { 
            ...result,
            currentBalance,
        };
    }
}

export { TransactionService };