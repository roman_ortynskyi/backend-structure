import {
    userRepository,
    transactionRepository,
    eventRepository,
    oddsRepository
} from '../data/repositories/repositories';
import { EventService } from './event/event.service';
import { TransactionService } from './transaction/transaction.service';
import { UserService } from './user/user.service';
  
const userService = new UserService({
  userRepository
});

const transactionService = new TransactionService({
  transactionRepository,
  userRepository
});

const eventService = new EventService({
  eventRepository,
  oddsRepository
});
  
export { 
  userService,
  transactionService,
  eventService
};
  