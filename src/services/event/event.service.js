class EventService {
    constructor({ eventRepository, oddsRepository }) {
        this._eventRepository = eventRepository;
        this._oddsRepository = oddsRepository;
    }

    async create(event) {
        const { 
            odds: {
                homeWin: home_win,
                awayWin: away_win,
                draw
            },
            awayTeam: away_team,
            homeTeam: home_team,
            startAt: start_at,
            type
        } = event;

        const createdOdds = await this._oddsRepository.create({
            home_win,
            away_win,
            draw
        });

        const createdEvent = await this._eventRepository.create({
            away_team,
            home_team,
            start_at,
            odds_id: createdOdds.id,
            type
        });

        return {
            ...createdEvent,
            odds: createdOdds
        };
    }
}

export { EventService };