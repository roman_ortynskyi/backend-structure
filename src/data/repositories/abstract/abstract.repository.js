class AbstractRepository {
  constructor({ db, model }) {
      this.db = db;
      this.model = model;
  }
    
  getAll() {
    return db
      .select()
      .table(this.model);
  }
    
  async getById(id) {
    const items = await this
      .db(this.model)
      .where({ id });
      
    return items[0];
  }
    
  async create(data) {
    const [result] = await this
      .db(this.model)
      .insert(data)
      .returning('*');

    return result;
  }
    
  async updateById(id, data) {
    const result = await this
      .db(this.model)
      .where({ id })
      .update({
        ...data,
        updated_at: new Date()
      })
      .returning('*');
    
    return result[0];
  }
    
  deleteById(id) {
    return this
      .db(this.model)
      .where({ id })
      .del();
  }
}

export { AbstractRepository };
