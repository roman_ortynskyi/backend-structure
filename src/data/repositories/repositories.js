import { db } from '../db/connection';
import { TransactionRepository } from './transaction/transaction.repository';
import { UserRepository } from './user/user.repository';
import { EventRepository } from './event/event.repository';
import { OddsRepository } from './odds/odds.repository';  

const userRepository = new UserRepository({
  db,
  model: 'user'
});

const transactionRepository = new TransactionRepository({
  db,
  model: 'transaction'
});

const eventRepository = new EventRepository({
  db,
  model: 'event'
});

const oddsRepository = new OddsRepository({
  db,
  model: 'odds'
});
  
export { 
  userRepository,
  transactionRepository,
  eventRepository,
  oddsRepository
};
  