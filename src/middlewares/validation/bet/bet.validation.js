import joi from 'joi';

const create = (req, res, next) => {
    const schema = joi.object({
      id: joi.string().uuid(),
      eventId: joi.string().uuid().required(),
      betAmount: joi.number().min(1).required(),
      prediction: joi.string().valid('w1', 'w2', 'x').required(),
    }).required();
      
    const validationResult = schema.validate(req.body);
    if(validationResult.error) {
        res.status(400).send({ error: validationResult.error.details[0].message });
        return;
    };

    next();
}

export const betValidation = {
    create
};