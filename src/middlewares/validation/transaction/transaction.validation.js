import joi from 'joi';

const create = (req, res, next) => {
    const schema = joi.object({
        id: joi.string().uuid(),
        userId: joi.string().uuid().required(),
        cardNumber: joi.string().required(),
        amount: joi.number().min(0).required(),
    }).required();
    const validationResult = schema.validate(req.body);
    if(validationResult.error) {
        res.status(400).send({ error: validationResult.error.details[0].message });
        return;
    };

    next();
}

export const transactionValidation = {
    create
};