import jwt from 'jsonwebtoken';

export const isAuth = async (req, res, next) => {
    const token = req.header('authorization').split(' ')[1];

    try {
        const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
        req.tokenPayload = tokenPayload;
        next();
    }
    catch(e) {
        return res.status(401).send({ error: 'Not Authorized' });
    }
};

export const isAdmin = async (req, res, next) => {
    const token = req.header('authorization').split(' ')[1];

    try {
        const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
        req.tokenPayload = tokenPayload;
        if (tokenPayload.type != 'admin') {
            throw new Error('Should be admin');
        }
        next();
    }
    catch(e) {
        return res.status(401).send({ error: 'Not Authorized' });
    }
};